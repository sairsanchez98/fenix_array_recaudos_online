

var Recaudo = new Vue({
    el: "#recaudos",
    data: {
        
        // DATOS GENERALES PARA AMBOS FORMULARIOS : 
        empresa:  "",
        ccrepresentante   : "",
        representante: "",
        servicio : "",
        descripcion  : "",
        valor_a_pagar : "",
        telefono :"",

        // 
        CamposValidados : false

        


    },
    methods: {

       
        Formulario : function (form){ 
                // formateamos 
                this.empresa =   "";
                this.ccrepresentante   =  "";
                this.representante =  "";
                this.servicio = "";
                this.descripcion  = "";
                this.valor_a_pagar = "";
                this.telefono  = "";
        
                // 
                this.CamposValidados = false;

              // Or with jQuery
              $(document).ready(function(){
                $('.modal').modal();
              });

            if(form == "FacturacionElectronica"){
                this.servicio = "FACTURACIÓN ELECTRÓNICA";
                this.descripcion ="SOFTWARE DE FACTURACIÓN ELECTRÓNICA FENIX";
                this.valor_a_pagar = 1350000;
                $('#modal_fac_electronica').modal('open'); 
            }else if(form == "personalizar"){
                this.servicio = "PAGO PERSONALIZADO";
                $('#modal_pago_personalizado').modal('open'); 
            }
        },


        ValidarCampos(form){
            if(form == "FacturacionElectronica"){
                if(this.empresa !== ""  && this.ccrepresentante !== "" && this.representante !== ""
                    && this.telefono !== ""){
                   this.CamposValidados = true; 
                }
            }else if(form == "personalizar"){
                if(this.empresa !== ""  && this.ccrepresentante !== "" && this.representante !== ""
                && this.telefono !== "" && this.descripcion !== ""){
                this.CamposValidados = true; 
                }
            }
        },

        SairPay : function () { // esta función controla y valida el pago de las matriculas
            
                $('#modal_fac_electronica').modal('close'); 
                $('#modal_pago_personalizado').modal('close'); 
                var handler = ePayco.checkout.configure({
                    key: '00e75cfc6480207a7135e6efd3b4bb3d',
                    test: true
                });
    
                /// EPAYCO VARIABLE PARA PAGO DE MATRÍCULA:
                var datos_pago={
                    //Parametros compra (obligatorio)
                    name: Recaudo.servicio ,
                    description: Recaudo.descripcion,
                    invoice:  "234s2342",// numero de factura
                    currency: "cop",
                    amount: Recaudo.valor_a_pagar ,
                    tax_base: "0",
                    tax: "0",
                    country: "co",
                    lang: "es",
          
                    //Onpage="false" - Standard="true"
                    external: "false",
            
                    //Atributos opcionales
                    extra1 : Recaudo.empresa,

                    confirmation: "http://secure2.payco.co/prueba_curl.php",
                    response: "#", // URL DE RESPUESTa
          
                    //Atributos cliente
                    name_billing: Recaudo.representante,
                    //address_billing: "Carrera 19 numero 14 91",
                    type_doc_billing: "cc",
                    mobilephone_billing: Recaudo.telefono,
                    number_doc_billing: Recaudo.ccrepresentante,
          
                   //atributo deshabilitación metodo de pago
                    methodsDisable: ["SP", "CASH" , "DP"]

          
                }
                handler.open(datos_pago);

            

        }

    },

})