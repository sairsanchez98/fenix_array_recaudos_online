<!DOCTYPE html>
<html lang="es">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Starter Template - Materialize</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  
</head>
<body>
    <nav>
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">
                <img src = "Assets/img/empresa/logo.png">
            </a>  
        </div>
    </nav>
 
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <h3 class="header center orange-text">PLATAFORMA DE RECAUDOS</h3>
      <div class="row center">
        <h5 class="header col s12 light">Estimado cliente, Ahora a través de nuestra plataforma 
            podrá pagar el valor de nuestros productos y servicios de forma rápida y segura.</h5>
      </div>
      <!-- <div class="row center">
        <a href="http://materializecss.com/getting-started.html" id="download-button" class="btn-large waves-effect waves-light orange">Get Started</a>
      </div> -->
      

    </div>
  </div>


  <div class="container" id="recaudos">
    <div class="section">

      <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m3"></div>
            
            <div class="col s12 m3">
                <div class="card ">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" src="Assets/img/servicios/facelec.svg">
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">Facturación electrónica<i class="material-icons right">more_vert</i></span>
                        <p>
                            <a @click="Formulario('FacturacionElectronica')" class="waves-effect waves-light btn-small"><i class="material-icons right">local_atm</i>Pagar</a>
                        </p>
                        <br>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">Facturación electrónica
                        <i class="material-icons right">close</i></span>
                        <p>Factura electrónicamente y sistematiza tus ventas, finanzas y contabilidad.</p>
                    </div>
                </div>
            </div>

            <div class="col s12 m3">
                <div class="card ">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img class="activator" src="Assets/img/servicios/personalizado.svg">
                    </div>
                    <div class="card-content">
                        <span class="card-title activator grey-text text-darken-4">Pago personalizado<i class="material-icons right">more_vert</i></span>
                        <p>
                            <a @click="Formulario('personalizar')" class="waves-effect waves-light btn-small"><i class="material-icons right">local_atm</i>Pagar</a>
                        </p>
                        <br>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">Pago personalizado
                        <i class="material-icons right">close</i></span>
                        <p>Ingrese sus datos, describa su pago y aplique un valor libre.</p>
                    </div>
                </div>
            </div>

            <div class="col s12 m3"></div>  

        </div>

    </div>
    <br><br>


    <!-- Modal FACTURACIÓN ELECTRÓNICA -->
    <div id="modal_fac_electronica" class="modal modal-fixed-footer">
        <div class="modal-content">
        <h4>Pago en línea</h4>
        <p>Software de Facturación electrónica</p>
        <form class="col s12">
            <div class="row">
                <div class="input-field col s12 m12">
                <input @keyup="ValidarCampos('FacturacionElectronica')" v-model="empresa" placeholder="Nombre de la empresa"  type="text" >
                <label >Empresa</label>
                </div>
                <div class="input-field col s12 m6">
                    <input @keyup="ValidarCampos('FacturacionElectronica')" v-model="ccrepresentante" type="number" placeholder = "ej: 123456789">
                    <label >Cédula del representante</label>
                </div>
                <div class="input-field col s12 m6">
                    <input @keyup="ValidarCampos('FacturacionElectronica')" v-model="representante" type="text" placeholder = "nombre y apellidos">
                    <label >Representante</label>
                </div>
                <div class="input-field col s12 m6">
                    <input @keyup="ValidarCampos('FacturacionElectronica')" v-model="telefono" type="number" placeholder = "celular o fijo">
                    <label >Teléfono</label>
                </div>
                
            </div>
            
        </form>
        </div>
        <div class="modal-footer">
        <a v-if="CamposValidados" @click="SairPay()" class="waves-effect waves-light btn-small">
            <i class="material-icons right">local_atm</i>Continuar</a>
        </div>
    </div>


    <!-- Modal PAGO PERSONALIZADO -->
    <div id="modal_pago_personalizado" class="modal modal-fixed-footer">
        <div class="modal-content">
        <h4>Pago en línea</h4>
        <p>Realizar pago personalizado </p>
        <form class="col s12">
            <div class="row">
                <div class="input-field col s12 m12">
                    <input @keyup="ValidarCampos('personalizar')" v-model="empresa" placeholder="Nombre de la empresa"  type="text" >
                    <label >Empresa</label>
                </div>
                <div class="input-field col s12 m6">
                    <input @keyup="ValidarCampos('personalizar')" v-model="ccrepresentante" type="number" placeholder = "ej: 123456789">
                    <label >Cédula del representante</label>
                </div>
                <div class="input-field col s12 m6">
                    <input @keyup="ValidarCampos('personalizar')" v-model="representante" type="text" placeholder = "nombre y apellidos">
                    <label >Representante</label>
                </div>
                <div class="input-field col s12 m6">
                    <input @keyup="ValidarCampos('personalizar')" v-model="telefono" type="number" placeholder = "celular o fijo">
                    <label >Teléfono</label>
                </div>

                
                <div class="input-field col s12 m12">
                    <input @keyup="ValidarCampos('personalizar')" v-model="descripcion"   type="text" >
                    <label >Describa su pago</label>
                </div>

                <div class="input-field col s12 m6">
                    <input @keyup="ValidarCampos('personalizar')" v-model="valor_a_pagar" type="number" placeholder = "ej: $250.000">
                    <label >Valor a pagar</label>
                </div>


                
            </div>
            
        </form>
        </div>
        <div class="modal-footer">
        <a v-if="CamposValidados" @click="SairPay()" class="waves-effect waves-light btn-small">
            <i class="material-icons right">local_atm</i>Continuar</a>
        </div>
    </div>


  </div>












  <footer class="page-footer orange">
    <!-- <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Company Bio</h5>
          <p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Settings</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">Connect</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div> -->
    <div class="footer-copyright">
      <div class="container">
      Desarrollado por <a class="orange-text text-lighten-3" href="https://array.com.co">ARRAY TIC SAS</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script src="Assets/vue.js"></script>
  <script src="Assets/axios.min.js"></script>
  <script type="text/javascript" src="https://checkout.epayco.co/checkout.js"></script>
  <script src="Controllers/JS/CtrlRecaudos.js"></script>



  </body>
</html>
